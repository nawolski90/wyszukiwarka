import React from 'react';

export default class Searchform extends React.Component {
	render() {
		return(
			<div className={this.props.isSelect ? "col-md-4" : "col-md-12"}>
				<input type='text' className="form-control" placeholder="wyszukaj kurs ..." onChange={this.props.filterList} />
		        <hr/>
		        <div className="list-group">
		          {this.props.list.map((course) => 
		            <a className={"list-group-item " + (this.props.isSelect === course ? "active" : "")} 
		            	key={course.id}
		            	onClick={()=>this.props.selected(course)} >

		              <h4 className="list-group-heading">{course.title}</h4>
		              <p className="list-group-text">{course.author}</p>

		            </a>
		          )}
		        </div>
			</div>
		)
	}
}





