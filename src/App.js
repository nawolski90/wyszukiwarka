import React from 'react';
import myCourses from './courses_data';
import Searchform from './searchform';
// import Courseactive from './course_active';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      courses: myCourses,
      query: '',
      search_list: [],
      selected: null
    }
  }

  select(course) {
    this.setState({
      selected: course
    })
  }

  filterList(event) {
    let query = event.target.value;

    this.setState({
      search_list: this.state.courses.filter((course) => 
          course.title.toLowerCase().includes(query.toLowerCase()) ||
          course.description.toLowerCase().includes(query.toLowerCase()) ||
          course.author.toLowerCase().includes(query.toLowerCase())
        
      )
    })
  }

  render() {
    return (
      <div className="container">
        <h1 className="text-center">Wyszukiwarka</h1>
        <hr/>
        <Searchform list={this.state.search_list}
                    filterList={this.filterList.bind(this)}
                    isSelect={this.state.selected}
                    selected={this.select.bind(this)}/>

        {this.state.selected ? <div className="col-md-8">
        <img src={this.state.selected.image} />
        <h4>{this.state.selected.title}</h4>
        <p>{this.state.selected.description}</p>
        <p>{this.state.selected.author}</p></div> : null}
      </div>
    );
  }
}

export default App;
